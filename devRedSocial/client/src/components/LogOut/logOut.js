import React, { Component } from 'react'
import { Link } from "react-router-dom"
import { sessionOff } from '../../store/actions/auth'
import { connect } from "react-redux";
import Button from "react-bootstrap/Button";


class LogOut extends Component {

  componentDidMount() {
    this.props.exit();
  }

  render() {
    return (
      <>

        <div className="containerItinerary" style={{ paddingTop: "20px", paddingBottom: "20px" }}>
          <p className='text-center text-success'>you have successfully logged out</p>
        </div>

        <div className="btn w-50 m-auto d-block">
          <Link to="/">
            <Button alt="button2" > Back Home </Button>
          </Link>
        </div>

      </>
    )
  }
}

const mapDispatchToProps = (dispatch) => ({
  exit: () => {
    dispatch(sessionOff())
  }
})

export default connect(null, mapDispatchToProps)(LogOut)
