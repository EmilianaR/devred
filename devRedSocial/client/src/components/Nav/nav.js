import { Switch, Menu, Icon, Avatar } from 'antd';
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
const { SubMenu } = Menu
const jwt = require("jsonwebtoken")

class SliderNav extends Component {
  state = {
    theme: 'light',
    avatar: "",
    name: ""
  }

  componentDidMount() {
    const token = localStorage.getItem('token');
    const tokenDecoded = jwt.decode(token);
    if (localStorage.getItem('success') === 'true') {
      const name = tokenDecoded.user.name;
      const avatar = tokenDecoded.user.img
      this.setState({ avatar: avatar });
      this.setState({ name })
    }
  }
  changeTheme = value => {
    this.setState({
      theme: value ? 'dark' : 'light',
    });
  };

  render() {
    return (
      <div className="pos-f-t">
        <nav className="navbar navbar-dark bg-dark d-flex flex-row">
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <span style={{ color: 'white' }}><Switch onChange={this.changeTheme} /> Change Theme </span>
        </nav>
        <div className="collapse" id="navbarToggleExternalContent">
          <Menu
            style={{ width: 256 }}
            defaultSelectedKeys={['1']}
            defaultOpenKeys={['sub1']}
            mode={this.state.mode}
            theme={this.state.theme}
          >
            <Menu.Item key="1">

              <Link to='/'><Icon type="home" />Home</Link>
            </Menu.Item>
            <Menu.Item key="2">
              <Icon type="calendar" />
              Navigation Two
          </Menu.Item>
            {
              localStorage.getItem('success') === 'true' ?
                <SubMenu
                  key="sub2"
                  title={
                    <span>
                      <Avatar size={40} src={this.state.avatar} />
                    </span>
                  }
                >
                  <Menu.Item key="7">Hello {this.state.name}!!</Menu.Item>
                  <Menu.Item key="8"><Link to="/logOut">Log Out</Link></Menu.Item>
                  <Menu.Item key="9">Option</Menu.Item>
                </SubMenu> :
                <SubMenu key='sub2'
                  title={
                    <Icon type="user" />
                  }>
                  <Menu.Item key="7"><Link to="/login">Log in</Link></Menu.Item>
                  <Menu.Item key="8"><Link to="/register">Create Account</Link></Menu.Item>
                  <Menu.Item key="9">Option 9</Menu.Item>
                </SubMenu>
            }
          </Menu>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return ({
    successStore: state.authReducer.success
  })
};

export default connect(mapStateToProps)(SliderNav)
