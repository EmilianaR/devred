import React, { Component } from 'react'
import './home.css'
import SliderNav from '../Nav/nav'
import { Button } from 'antd';

export default class Home extends Component {
  render() {
    return (
      <>
        <SliderNav />
        <div className='container-home d-flex flex-column justify-content-center align-items-center'>
          <Button type="danger" size='large'>
            EXPLORE
    </Button>
        </div>
      </>
    )
  }
}
