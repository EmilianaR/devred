import React, { Component } from 'react'
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import { connect } from 'react-redux'
import { getAuth } from '../../store/actions/auth'
import AlertModel from '../Alert/alert'
import { Link, Redirect } from "react-router-dom"
import './login.css'
import SliderNav from '../Nav/nav'

class Login extends Component {
  state = {
    password: "",
    email: "",
    errors: [],
    showErrors: false,
    redirect: false
  }

  componentDidUpdate(prevProps) {
    if (this.props.success !== prevProps.success) {
      this.setState({
        redirect: this.props.success
      });
    }
  }

  renderRedirect = () => {
    if (this.state.redirect === true) {
      return <Redirect to='/' />
    }
  }

  setPass(e) {
    this.setState({ password: e })
  }

  setEmail(e) {
    this.setState({ email: e })
  }

  getLogin(e) {
    let user = {
      email: this.state.email,
      password: this.state.password
    }
    this.props.login(user)
    if (this.props.errors) {
      this.setState({
        showErrors: true,
        errors: this.props.errors
      })
    }
  }

  render() {
    return (
      <>
        <SliderNav />
        <div className='d-flex flex-column justify-content-center align-items-center container-fondoImg'>
          {(this.state.showErrors && this.props.errors ?
            <AlertModel alerts={this.props.errors} alertType="error" />
            :
            <div></div>)}
          <Form className="login-form p-3 bg-white container-form">
            <Form.Item>
              <Input
                prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder="Username"
                type="email"
                value={this.state.email}
                onChange={(e) => this.setEmail(e.target.value)}
              />
            </Form.Item>
            <Form.Item>
              <Input
                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                type="password"
                placeholder="Password"
                value={this.state.password}
                onChange={(e) => this.setPass(e.target.value)}
              />
            </Form.Item>
            <Form.Item>
              <div className='d-flex justify-content-around flex-column'>
                <Checkbox>Remember me</Checkbox>
                <p className="login-form-forgot">
                  Forgot password
                </p>
              </div>
            </Form.Item>
            {this.renderRedirect()}
            <Form.Item>
              <Button type="primary" htmlType="submit" className="login-form-button" onClick={(e) => this.getLogin(e)}>
                Log in
        </Button>
            </Form.Item>
            <Form.Item>
              Or <Link to='/register'>Register now!</Link>
            </Form.Item>
          </Form>
        </div>
      </>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    success: state.authReducer.success,
    token: state.authReducer.token,
    errors: state.authReducer.errors
  }
};

const mapDispatchToProps = (dispatch) => ({
  login: (user) => {
    dispatch(getAuth(user))
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
