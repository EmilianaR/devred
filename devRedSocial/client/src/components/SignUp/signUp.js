import React, { Component } from 'react'
import {connect} from 'react-redux'
import createAccount from '../../store/actions/register'
import SliderNav from '../Nav/nav'
import '../Login/login.css'
import {
  Form,
  Input,
  Button,
  Avatar,
  Icon, 
  Tooltip
} from 'antd';


class SignUp extends Component {
  state = {
    confirmDirty: false,
    profilePic: "",
    email: "",
    password: "",
    name: ""
  };

  setPass(e) {
    this.setState({ password: e })
  }

  setEmail(e) {
    this.setState({ email: e })
  }

  setName(e) {
    this.setState({ name: e })
  }

  setAvatar(e) {
    this.setState({ profilePic: e })
  }


  handleConfirmBlur = e => {
    const { value } = e.target;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  };

  getNewUser(e){
    let newUser ={
      email: this.state.email,
      password: this.state.password,
      name: this.state.name,
      profilePic: this.state.profilePic
    }
    this.props.addUser(newUser)
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    
    return (
      <>
      <SliderNav/>
      <div className='d-flex flex-column justify-content-center align-items-center w-100'>
        <div className='p-3'>
          {(this.state.profilePic !== "") ? <Avatar size={200} src={this.state.profilePic} /> :
            <Avatar size={200} icon={<Icon type="user" />} />}
        </div>
        <Form className='p-3'>
          <Form.Item
            label={<span>
              Url image&nbsp;
              <Tooltip title="Do you want to add a photo?">
                <Icon type="question-circle-o" />
              </Tooltip>
            </span>}>
            <Input type='text' value={this.state.profilePic} onChange={(e) => this.setAvatar(e.target.value)} />
          </Form.Item>
          <Form.Item label="E-mail">
            {getFieldDecorator('email', {
              initialValue: this.state.email,
              rules: [
                {
                  type: 'email',
                  message: 'The input is not valid E-mail!',
                },
                {
                  required: true,
                  message: 'Please input your E-mail!',
                },
              ],
            })(<Input onChange={(e) => this.setEmail(e.target.value)} />)}
          </Form.Item>
          <Form.Item label="Password" hasFeedback>
            {getFieldDecorator('password', {
              initialValue: this.state.password,
              rules: [
                {
                  required: true,
                  message: 'Please input your password!',
                },
                {
                  validator: this.validateToNextPassword,
                },
              ],
            })(<Input.Password onChange={(e) => this.setPass(e.target.value)} />)}
          </Form.Item>
          <Form.Item label="Confirm Password" hasFeedback>
            {getFieldDecorator('confirm', {
              rules: [
                {
                  required: true,
                  message: 'Please confirm your password!',
                },
                {
                  validator: this.compareToFirstPassword,
                },
              ],
            })(<Input.Password onBlur={this.handleConfirmBlur} />)}
          </Form.Item>
          <Form.Item
            label='Name'
          >
            {getFieldDecorator('Name', {
              initialValue: this.state.name,
              rules: [{ required: true, message: 'Please input your name!', whitespace: true }],
            })(<Input  onChange={(e) => this.setName(e.target.value)} />)}
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" onClick={(e) => this.getNewUser(e)}>
              Register
          </Button>
          </Form.Item>
        </Form>
      </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    success: state.register.success,
    token: state.register.token,
    errors: state.register.errors
  }
};

const mapDispatchToProps = (dispatch) => ({
  addUser: (user) => {
    dispatch(createAccount(user))
  }
});

const WrappedRegistrationForm = Form.create({ name: 'register' })(SignUp);
export default connect(mapStateToProps, mapDispatchToProps)(WrappedRegistrationForm)