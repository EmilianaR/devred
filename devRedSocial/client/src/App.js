import React, {Component} from 'react';
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import Home from './components/Home/home'
import Login from './components/Login/login'
import  WrappedRegistrationForm from './components/SignUp/signUp'
import LogOut from './components/LogOut/logOut'

// Bootstrap
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/js/bootstrap.bundle.js";
import "bootstrap/dist/js/bootstrap.bundle.min";

//AntDesign
import "antd/dist/antd.js"
import "antd/dist/antd.css"
import "antd/dist/antd.min.css"

export default class App extends Component {
  render() {
    return (
      <Router>
        {/* <AlertModel/> */}
        <Switch>
          <Route exact path='/' component ={Home}/>
          <Route path='/login' component= {Login}></Route>
          <Route path='/register' component = { WrappedRegistrationForm}></Route>
          <Route path='/logOut' component={LogOut}></Route>
        </Switch>
      </Router>
    )
  }
}

