import {SIGN_UP} from '../types'

const createAccount = (newUser) => {
  return async function (dispatch) {
    try {
      const data = await sendFetch(newUser)
      const dataFetched = resultFetch(data)
      return dispatch(dataFetched)
    } catch (error) {
      console.error(error.message)
    }
  }
}

const sendFetch = async (newUser) => {
  let res = await fetch('http://localhost:5000/api/users', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    },
    body: JSON.stringify(newUser)
  })

  let data = await res.json();
  console.log("data", data)
  return data;
}

const resultFetch = (data) => {
  return {
    type: SIGN_UP,
    payload: {
      success: data.success,
      errors: data.errors,
      token: data.token
    }
  }
}

export default createAccount