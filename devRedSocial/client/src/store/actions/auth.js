import { SESSION_ON, SESSION_OFF } from '../types'

export const getAuth = (user) => {
  return async function (dispatch) {
    try {
      const data = await sendFetch(user)
      const dataFetched = resultFetch(data)
      return dispatch(dataFetched)
    } catch (error) {
      console.error(error.message)
    }
  }
}

const resultFetch = (data) => {
  return {
    type: SESSION_ON,
    payload: {
      success: data.success,
      errors: data.errors,
      token: data.token
    }
  }
}


const sendFetch = async (user) => {
  let res = await fetch('http://localhost:5000/api/auth', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    },
    body: JSON.stringify(user)
  })

  let data = await res.json();
  console.log("data", data)
  console.log(localStorage.getItem('success'))
  return data;
}

export const sessionOff = () => {
  return ({
    type: SESSION_OFF,
    payload: {
      success: false,
      token: ''
    }
  })
}