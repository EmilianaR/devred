import { SESSION_ON, SESSION_OFF } from '../types'

const initialState = {
  errors: "",
  token: "",
  success: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SESSION_ON:
      localStorage.setItem('success', action.payload.success)
      localStorage.setItem('token', action.payload.token)
      return {
        ...state,
        success: action.payload.success,
        token: action.payload.token,
        errors: action.payload.errors
      }
    case SESSION_OFF:
      localStorage.clear()
      return {
        ...state,
        success: action.payload.success,
        token: action.payload.token,
      }
    default:
      return state;
  }
}