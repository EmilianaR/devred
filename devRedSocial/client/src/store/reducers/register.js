import {SIGN_UP} from '../types'

const initialState = {
  errors: "",
  token: "",
  success: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SIGN_UP:
      localStorage.setItem('success', action.payload.success)
      localStorage.setItem('token', action.payload.token)
      return {
        ...state,
        success: action.payload.success,
        token: action.payload.token,
        errors: action.payload.errors
      }
    default:
      return state;
  }
}