import { combineReducers } from "redux";
import authReducer from './authReducer'
import register from './register'

const rootReducer = combineReducers({
    authReducer,
    register
})

export default rootReducer